const mongosse = require('../config/database');
const mongoose = require('../config/database');

const Schema = mongosse.Schema;

const currentDate = new Date();

const DemandSchema = new Schema({
    loja_id: { type: String, default: '111.111.111-11' },
    client: { type: String, required: true },
    title: { type: String, required: true },
    description: { type: String, required: true },
    street: { type: String, required: true },
    number: { type: Number, required: true },
    neighboard: { type: String, required: true },
    public_place: { type: String, required: true },
    done: { type: Boolean, default: false },
    date: { type: String, default: currentDate.getDate() + '-' + (currentDate.getMonth() + 1) + '-' + currentDate.getFullYear() },
    created: { type: Date, default: Date.now()}
});

module.exports = mongosse.model('Demand', DemandSchema);

