const express = require('express');
var cors = require('cors')

const router = express.Router();
router.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
    router.use(cors());
    next();
})

const DemandController = require('../controller/DemandController');

const DemandValidation = require('../middlewares/demanValidation');

router.post('/', DemandValidation, DemandController.create);
router.put('/:id', DemandValidation, DemandController.update);
router.post('/filter/all', DemandController.all)
router.delete('/:id', DemandController.delete)

router.post('/filter/today', DemandController.toDay)

module.exports = router;